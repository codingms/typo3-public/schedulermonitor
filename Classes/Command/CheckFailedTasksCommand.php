<?php
declare(strict_types=1);
namespace CodingMs\Schedulermonitor\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2020 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Schedulermonitor\Utility\MailUtility;
use CodingMs\Schedulermonitor\Repository\SchedulerTaskRepository;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CheckFailedTasksCommand extends Command
{
    /**
     * configure command
     */
    protected function configure()
    {
        $this->setDescription('Checks for failed scheduler tasks and sends an email if found.');
        $this->addArgument('subject', InputArgument::REQUIRED, 'Email subject.');
        $this->addArgument('email', InputArgument::REQUIRED, 'Receiver email address.');
        $this->addArgument(
            'maxDuration',
            InputArgument::OPTIONAL,
            'Max. time (in seconds) a task is allowed to run before a warning is sent. '
            . 'After this time the process is supposed to be dead. '
            . 'Note: By default the scheduler removes the logged "executions" of tasks after 24 hours (extension setting). '
            . 'After that the schedulermonitor will not find dead tasks anymore. ',
            3600
        );
    }

    /**
     * Checks for failed scheduler tasks and sends an email if found.
     *
     * This task checks if there are error messages stored in the scheduler tasks table and sends them via email. Emails
     * can be comma-separated.
     *
     * @param string $subject
     * @param string $email
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $subject = $input->getArgument('subject');
        $email = $input->getArgument('email');
        $maxDuration = intval($input->getArgument('maxDuration'));
        $maxDuration = $maxDuration > 0 ? $maxDuration : 0;

        /** @var SchedulerTaskRepository $tasksRepository */
        $tasksRepository = GeneralUtility::makeInstance(SchedulerTaskRepository::class);

        $rows = $tasksRepository->findAllWithErrorsNotSent();
        if ($rows) {
            foreach ($rows as $row) {
                /** @var AbstractTask $task */
                $task = unserialize($row['serialized_task_object']);
                $message =
                    'Task threw an error: ' . get_class($task) . '\' (UID: ' . $task->getTaskUid() . '). '
                    . 'Was started at ' . date('Y-m-d H:i:s', $task->getExecutionTime()) . '. '
                    . 'Error: ' . $row['lastexecution_failure'] . ' '
                ;
                $output->writeln($message);
                $this->sendErrorMessage($subject, $email, $message, $task);
                $tasksRepository->updateErrorSentTimestamp($task->getTaskUid());
            }
        }

        $rows = $tasksRepository->findAllRunningTooLong($maxDuration);
        if ($rows) {
            foreach ($rows as $row) {
                /** @var AbstractTask $task */
                $task = unserialize($row['serialized_task_object']);
                $durationSeconds = $GLOBALS['EXEC_TIME'] - $row['taskStartTime'];
                $message =
                    'Assuming that this process is dead. Execution of \'' . get_class($task)
                    . '\' (UID: ' . $task->getTaskUid() . ') was started at ' . date('Y-m-d H:i:s', $task->getExecutionTime()) . '. '
                    . 'It is running since ' . sprintf('%02d:%02d:%02d', ($durationSeconds/ 3600),($durationSeconds/ 60 % 60), $durationSeconds% 60);
                $output->writeln($message);
                $this->sendErrorMessage($subject, $email, $message, $task);
            }
        }

        return 0;
    }

    /**
     * @param $subject
     * @param $email
     * @param $message
     * @param $task AbstractTask
     * @throws Exception
     */
    public function sendErrorMessage($subject, $email, $message, $task)
    {
        /** @var MailUtility $mailUtility */
        $mailUtility = GeneralUtility::makeInstance(MailUtility::class);

        $messageArray = [
            'message' => $message,
            'time' => date('d.m.Y H:i', $task->getExecutionTime()),
            'task' => serialize($task),
        ] ;

        $mailUtility->sendEmail(
            $subject,
            json_encode($messageArray,JSON_PRETTY_PRINT),
            $email
        );
    }
}
