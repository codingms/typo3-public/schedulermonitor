<?php
declare(strict_types=1);
namespace CodingMs\Schedulermonitor\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2024 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\TemplatePaths;

class SendTestMailCommand extends Command
{
    use LoggerAwareTrait;

    public function __construct(?string $name = null)
    {
        $this->logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
        parent::__construct($name);
    }

    /**
     * configure command
     */
    protected function configure()
    {
        $this->setDescription(
            'Sends out a test email. Can be used to monitor the email sending functionality of the'
            . ' TYPO3 instance. Replaces the scheduler test mail which has been removed in TYPO3 12.'
        );

        $this->addOption(
            'email',
            'e',
            InputOption::VALUE_REQUIRED,
            'Receiver email address'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = $input->getOption('email');
        if (empty($email)) {
            $output->writeln('<error>You must provide a valid email address</error>');
            return self::FAILURE;
        }
        return $this->sendTestMail($email) ? self::SUCCESS : self::FAILURE;
    }

    protected function sendTestMail(string $email)
    {
        if (!empty($email)) {
            // If an email address is defined, send a message to it
            $this->logger->info('Test email sent to "{email}"', ['email' => $email]);

            $templateConfiguration = $GLOBALS['TYPO3_CONF_VARS']['MAIL'];
            $templateConfiguration['templateRootPaths'][20] = 'EXT:schedulermonitor/Resources/Private/Templates/Email/';

            if (Environment::isCli()) {
                $calledBy = 'CLI module dispatcher';
            } else {
                $calledBy = 'TYPO3 backend';
            }
            $fluidMail = GeneralUtility::makeInstance(
                FluidEmail::class,
                GeneralUtility::makeInstance(TemplatePaths::class, $templateConfiguration)
            );
            $fromAddress = !empty($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'])
                ? $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress']
                : $email;
            $fluidMail
                ->to($email)
                ->subject('[SCHEDULERMONITOR] Test')
                ->from(new Address($fromAddress, 'TYPO3-Schedulermonitor'))
                ->setTemplate('TestTask')
                ->assignMultiple(
                    [
                        'data' => [
                            'calledBy' => $calledBy,
                            'tstamp' => time(),
                        ],
                    ]
                );
            /** @var Mailer $mailer */
            $mailer = GeneralUtility::makeInstance(Mailer::class);
            $mailer->send($fluidMail);
            return true;
        }
        // No email defined, just log the task
        $this->logger->warning('No email address given');

        return false;
    }
}
