<?php
declare(strict_types=1);
namespace CodingMs\Schedulermonitor\Utility;

use Exception;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2020 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class MailUtility {

    /**
     * @param $subject
     * @param $body
     * @param $to
     * @return bool
     * @throws Exception
     */
    public function sendEmail($subject, $body, $to)
    {
        $receivers = GeneralUtility::trimExplode(',', $to);
        foreach ($receivers as $receiver) {
            if (!GeneralUtility::validEmail($receiver)) {
                throw (new Exception('No valid email provided.'));
            }
        }
        /** @var MailMessage $mail */
        $mail = GeneralUtility::makeInstance(MailMessage::class);
        $mail->setTo($receivers);
        $mail->setSubject($subject);

        // Constant "TYPO3_version" has been removed with TYPO3 12
        // https://docs.typo3.org/c/typo3/cms-core/main/en-us/Changelog/10.3/Deprecation-90007-GlobalConstantsTYPO3_versionAndTYPO3_branch.html
        if (class_exists('\TYPO3\CMS\Core\Information\Typo3Version') && GeneralUtility::makeInstance(\TYPO3\CMS\Core\Information\Typo3Version::class)->getMajorVersion() == 12) {
            /** @noinspection PhpUndefinedMethodInspection */
            $mail->text($body);
        } else {
            if ((int)TYPO3_version >= 10) {
                /** @noinspection PhpUndefinedMethodInspection */
                $mail->text($body);
            } else {
                /** @noinspection PhpUndefinedMethodInspection */
                $mail->setBody($body);
            }
        }
        return (bool)$mail->send();
    }
}
