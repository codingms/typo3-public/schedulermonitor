<?php
declare(strict_types=1);
namespace CodingMs\Schedulermonitor\Repository;
use Doctrine\DBAL\Driver\Statement;
use PDO;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Expression\ExpressionBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2020 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class SchedulerTaskRepository {

    public function findAllWithErrors()
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_scheduler_task');
        return $queryBuilder
            ->select('*')
            ->from('tx_scheduler_task')
            ->andWhere(
                $queryBuilder->expr()->neq('lastexecution_failure', $queryBuilder->createNamedParameter('')),
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, PDO::PARAM_INT))
            )
            ->executeQuery()
            ->fetchAllAssociative();
    }

    public function findAllWithErrorsNotSent()
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_scheduler_task');
        return $queryBuilder
            ->select('*')
            ->from('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->eq('tx_schedulermonitor_errorsent', $queryBuilder->createNamedParameter(0, PDO::PARAM_INT))
            )
            ->orWhere(
                $queryBuilder->quoteIdentifier('lastexecution_time')
                . ExpressionBuilder::GT
                . $queryBuilder->quoteIdentifier('tx_schedulermonitor_errorsent')
            )
            ->andWhere(
                $queryBuilder->expr()->neq('lastexecution_failure', $queryBuilder->createNamedParameter(''))
            )
            ->executeQuery()
            ->fetchAllAssociative();
    }

    /**
     * @param int $maxDuration
     * @return array
     */
    public function findAllRunningTooLong($maxDuration = 3600): array
    {
        $tasksRunningTooLong = [];

        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_scheduler_task');

        // Select all tasks with executions
        $result = $queryBuilder->select('uid', 'serialized_executions', 'serialized_task_object')
            ->from('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->neq(
                    'serialized_executions',
                    $queryBuilder->createNamedParameter('', \PDO::PARAM_STR)
                ),
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->executeQuery();

        while ($row = $result->fetchAssociative()) {
            if ($serialized_executions = unserialize($row['serialized_executions'])) {
                foreach ($serialized_executions as $taskStartTime) {
                    $durationSeconds = $GLOBALS['EXEC_TIME'] - $taskStartTime;
                    if ($durationSeconds > $maxDuration) {
                        $row['taskStartTime'] = $taskStartTime;
                        $tasksRunningTooLong[] = $row;
                    }
                }
            }
        }
        return $tasksRunningTooLong;
    }

    /**
     * @param int $uid
     * @return Statement|int
     */
    public function updateErrorSentTimestamp(int $uid) {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_scheduler_task');
        return $queryBuilder
            ->update('tx_scheduler_task')
            ->where(
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, PDO::PARAM_INT))
            )
            ->set('tx_schedulermonitor_errorsent', time())
            ->executeStatement();
    }
}
