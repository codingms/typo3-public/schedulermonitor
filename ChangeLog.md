# Schedulermonitor Changelog

* [DOCS] Fix documentation

## 2024-09-26  Release of version 1.4.0

* [TASK] Improve TYPO3 12 compatibility, remove deprecated Commands.php, enhance type annotations
* [FEATURE] Add command to send out a test email

## 2024-02-28  Release of version 1.3.4

* [TASK] Make extension compatible with TYPO3 12

## 2024-02-28  Release of version 1.3.3

* [DOCS] Update documentation
* [BUGFIX] Fix version constraint in ext_emconf.php to match composer.json

## 2022-06-29  Release of version 1.3.2

* [TASK] Fix link to german readme file

## 2022-06-29  Release of version 1.3.1

* [TASK] Refactor documentation

## 2022-05-30  Release of version 1.3.0

* [TASK] Add test command which only throws an exception
* [TASK] Make extension compatible with TYPO3 11
* [BUGFIX] Fix "extra" section in composer.json

## 2021-03-08  Release of version 1.2.0

* [FEATURE] Check for tasks assumed to be dead (running too long)

## 2020-11-26  Release of version 1.1.0

* [TASK] Migrate CommandController to symfony command to be compatible with TYPO3 9 and 10
* [BUGFIX] Fix typo in extension name

## 2020-10-07  Initial release 1.0.0

* [FEATURE] Check if there are failed scheduler tasks and if yes send a mail
