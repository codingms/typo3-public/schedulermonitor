# Schedulermonitor Erweiterung für TYPO3

Überwacht Planer-Tasks und versendet eine E-Mail, wenn bei der Durchführung Fehler auftreten oder diese zu lange laufen.

Stellt außerdem einen Befehl zur Verfügung, der eine Testmail versendet, z. B. um die E-Mail-Funktionalität der TYPO3-Instanz zu überwachen. Diese Funktion kann als Ersatz für die Testmail-Funktion aus EXT:scheduler dienen, die mit TYPO3 12 entfernt wurde.

Wenn ein zusätzliches oder individuelles Feature benötigt wird - nimm Kontakt auf!

**Links:**

*   [Schedulermonitor Dokumentation](https://www.coding.ms/documentation/typo3-scheduler-monitor "Schedulermonitor Dokumentation")
*   [Schedulermonitor Bug-Tracker](https://gitlab.com/codingms/typo3-public/schedulermonitor/-/issues "Schedulermonitor Bug-Tracker")
*   [Schedulermonitor Repository](https://gitlab.com/codingms/typo3-public/schedulermonitor "Schedulermonitor Repository")
*   [TYPO3 Scheduler-Monitor](https://www.coding.ms/typo3-extensions/typo3-schedulermonitor/ "TYPO3 Scheduler-Monitor")
