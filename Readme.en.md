# Schedulermonitor Extension for TYPO3

Monitors scheduler tasks and sends an email if a task has failed or is running too long.

Additionally, provides a command which sends out a test email. Can be used to monitor the email sending functionality of the TYPO3 instance. Replaces the scheduler test mail which has been removed in TYPO3 12.

If you need some additional or custom feature - get in contact!

**Links:**

*   [Schedulermonitor Documentation](https://www.coding.ms/documentation/typo3-scheduler-monitor "Schedulermonitor Documentation")
*   [Schedulermonitor Bug-Tracker](https://gitlab.com/codingms/typo3-public/schedulermonitor/-/issues "Schedulermonitor Bug-Tracker")
*   [Schedulermonitor Repository](https://gitlab.com/codingms/typo3-public/schedulermonitor "Schedulermonitor Repository")
*   [TYPO3 Scheduler monitor](https://www.coding.ms/typo3-extensions/typo3-schedulermonitor/ "TYPO3 Scheduler monitor")
