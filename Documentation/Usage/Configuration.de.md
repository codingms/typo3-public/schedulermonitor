# Konfiguration

Der Monitoring-Task wird wie folgt im Planer hinzugefügt:

* Task mit der Klasse "Konsolenbefehle ausführen" und dem "Planbaren Befehl" `schedulerMonitor:checkForFailedTasks` anlegen.
* Die Frequenz wie gewünscht einstellen, z. B. alle fünf Minuten.
* E-Mail-Adresse hinterlegen (mehrere sind komma-getrennt möglich) und den Betreff.
* Zeit einstellen, nach der ein Task als "tot" angesehen wird. Standard ist 3600 Sekunden (1 Stunde).

Hinweis: Standardmäßig entfernt der Planer die aufgezeichneten "executions" nach 24 Stunden (einstellbar in EXT:scheduler). Danach werden keine "toten" Tasks mehr gefunden.

Um Test-Mails zu versenden:

* Task mit der Klasse "Konsolenbefehle ausführen" und dem "Planbaren Befehl" `schedulerMonitor:sendTestMail` anlegen.
* Die Frequenz wie gewünscht einstellen, z. B. einmal pro Tag.
* Empfänger-E-Mail-Adresse hinterlegen (eine einzelne Adresse).
