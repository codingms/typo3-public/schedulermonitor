# Configuration

Add the monitoring scheduler task and configure it by following these steps:

* Select class "Execute console commands" and add the task `schedulerMonitor:checkForFailedTasks`.
* Let it run as often as you want to check for failed tasks, e.g. every five minutes.
* Set the email address (more than one email address can be provided comma-separated) and the email subject.
* Set time after a task should be assumed as dead. Default is 3600 seconds (1 hour).

Note: By default the scheduler removes the logged "executions" of tasks after 24 hours (setting in EXT:scheduler). After that the schedulermonitor will not find dead tasks anymore.

To send out test emails:

* Select class "Execute console commands" and add the task `schedulerMonitor:sendTestMail`.
* Let it run as often as you want to send out mails, e.g. once per day.
* Set the receiver email address (only a single address).
