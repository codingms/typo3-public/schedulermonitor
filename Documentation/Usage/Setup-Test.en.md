# Test the setup

There is also a command `schedulerMonitor:throwError` which does nothing except throwing an error. You can add this command to the scheduler via "Execute console commands" to test if everything works as expected and you get the notification email.
