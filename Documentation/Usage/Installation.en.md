# Installation

1. Install the extension via extension manager or composer:
```
composer req codingms/schedulermonitor
```

2. Execute "Analyse database" in the maintanance module or run `vendor/bin/typo3cms database:update` if you have `typo3_console` installed because the extension adds a database field.
