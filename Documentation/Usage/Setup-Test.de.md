# Konfiguration testen

Die Erweiterung liefert den Konsolenbefehl `schedulerMonitor:throwError` mit, welcher ausschließlich einen Fehler wirft. Dieser Befehl kann mittels "Planbarem Befehl" dem Planer hinzugefügt werden um zu Testen, ob alles korrekt eingerichtet ist und die E-Mail versendet wird.
