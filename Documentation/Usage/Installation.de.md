# Installation

1. Die Extension per Extension-Manager oder per Composer installieren:
```
composer req codingms/schedulermonitor
```

2. Im Wartungsmodul die "Datenbankanalyse" oder `vendor/bin/typo3cms database:update` ausführen, wenn `typo3_console` installiert ist, denn die Erweiterung fügt ein Datenbankfeld hinzu.
